// ふわっとした動き
$(function () {
    if (location.href.match(/course/)) {
        $("#appear-btn").fadeIn("fast");
        $("#appear-btn").css({
            "position": "fixed",
        });

    } else {

        $(window).on("scroll", function () {
            if ($(this).scrollTop() > 800) {
                $("#appear-btn").fadeIn("fast");
            } else {
                $("#appear-btn").fadeOut("fast");
            }
            var docHeight = $(document).innerHeight(), //ドキュメントの高さ
                windowHeight = $(window).innerHeight(), //ウィンドウの高さ
                pageBottom = docHeight - windowHeight; //ドキュメントの高さ - ウィンドウの高さ

            var html = window.document.documentElement;

            //スクロールされた量とフッターの高さがページ全体の高さ以上なったら、positionの値がabsoluteになるように指定
            if (html.scrollHeight - html.scrollPosition <= html.footHeight) {
                $("#appear-btn").css({
                    "position": "absolute",
                });
            } else {
                $("#appear-btn").css({
                    "position": "fixed",
                });
            }
        });
    }
});
